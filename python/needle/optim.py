"""Optimization module"""
import needle as ndl
import numpy as np


class Optimizer:
    def __init__(self, params):
        self.params = params

    def step(self):
        raise NotImplementedError()

    def reset_grad(self):
        for p in self.params:
            p.grad = None


class SGD(Optimizer):
    def __init__(self, params, lr=0.01, momentum=0.0, weight_decay=0.0):
        super().__init__(params)
        self.lr = lr
        self.momentum = momentum
        self.u = [ndl.init.zeros(*p.shape, dtype=p.dtype) for p in params]
        self.weight_decay = weight_decay

    def step(self):
        ### BEGIN YOUR SOLUTION
        g = [(ndl.Tensor(p.grad.detach(), dtype=p.dtype)
              + self.weight_decay * p.detach())
             for p in self.params]
        self.u = [self.momentum * uu + (1 - self.momentum) * gg
                  for (uu, gg) in zip(self.u, g)]
        for (p, uu) in zip(self.params, self.u):
            p.data = p - self.lr * uu
        ### END YOUR SOLUTION


class Adam(Optimizer):
    def __init__(
        self,
        params,
        lr=0.01,
        beta1=0.9,
        beta2=0.999,
        eps=1e-8,
        weight_decay=0.0,
    ):
        super().__init__(params)
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.eps = eps
        self.weight_decay = weight_decay
        self.t = 0

        self.m = [ndl.init.zeros(*p.shape, dtype=p.dtype) for p in params]
        self.v = [ndl.init.zeros(*p.shape, dtype=p.dtype) for p in params]

    def step(self):
        ### BEGIN YOUR SOLUTION
        self.t += 1

        g = [(ndl.Tensor(p.grad.detach(), dtype=p.dtype)
              + self.weight_decay * p.detach())
             for p in self.params]
        self.m = [self.beta1 * mm + (1 - self.beta1) * gg
                  for (mm, gg) in zip(self.m, g)]
        self.v = [self.beta2 * vv + (1 - self.beta2) * gg**2
                  for (vv, gg) in zip(self.v, g)]

        m_hat = [mm / (1 - self.beta1**self.t) for mm in self.m]
        v_hat = [vv / (1 - self.beta2**self.t) for vv in self.v]

        for (p, mm_hat, vv_hat) in zip(self.params, m_hat, v_hat):
            p.data -= self.lr * mm_hat / (vv_hat**0.5 + self.eps)
        ### END YOUR SOLUTION
