from functools import reduce
import gzip
import numpy as np
import struct
from .autograd import Tensor

from typing import Iterator, Optional, List, Sized, Union, Iterable, Any


class Transform:
    """Interface for a size-preserving image transform.

    A `Transform` is simply a callable whose input is an NDArray of shape
    `(H, W, C)`, where `H` is the height in pixels, `W` is the width in pixels,
    and `C` is the number of color channels.  The output is an NDarray of the
    same shape `(H, W, C)` representing the transformed image.
    """
    def __call__(self, x):
        raise NotImplementedError


class RandomFlipHorizontal(Transform):
    """
    Horizonally flips the given image with probability `p` (supplied to the
    constructor).  Otherwise (with probability `1 - p`), returns the given
    image unchanged.
    """
    def __init__(self, p = 0.5):
        self.p = p

    def __call__(self, img):
        # Note: use the line below as the only source of randomness, or else
        # the course-supplied unit tests will break.
        flip_img = np.random.rand() < self.p
        ### BEGIN YOUR SOLUTION
        if flip_img:
            return np.flip(img, axis=1)
        else:
            return img
        ### END YOUR SOLUTION


class RandomCrop(Transform):
    """Zero-pads the given image by `padding` (supplied to the constructor)
    pixels on each edge and then randomly crops the image back to size.

    This transformation can also be thought of as a random translation of the
    viewfinder.
    """
    def __init__(self, padding=3):
        self.padding = padding

    def __call__(self, img):
        # Note: shift the image by shift_x, shift_y as defined below or else
        # the course-supplied unit tests will break.

        # Response note: I swapped `shift_x` and `shift_y` so that they would
        # represent horizontal and vertical shifts respectively (as I think is
        # the common convention) without breaking the unit tests which assume a
        # certain order of randum number consumption (makes some sense in a
        # course, but not in the real world).
        shift_y, shift_x = np.random.randint(low=-self.padding,
                                             high=self.padding+1, size=2)
        ### BEGIN YOUR SOLUTION
        return np.pad(img, pad_width=((self.padding, self.padding),
                                      (self.padding, self.padding),
                                      (0, 0)))[
                (self.padding + shift_y) : (self.padding + shift_y + img.shape[0]),
                (self.padding + shift_x) : (self.padding + shift_x + img.shape[1]),
                : ]
        ### END YOUR SOLUTION


class Dataset:
    r"""An abstract class representing a dataset.

    For our purposes, a dataset is a collection of "examples."  An example is
    typically a pair `(datum, label)` as used in supervised model training, but
    can in general be a tuple of any length whose elements are arrays.

    Subclasses should override `__len__` (the number of examples in the
    dataset) and `__getitem__`.  The implementation of `__getitem__(i)` should
    support `i` being either a scalar (integer) or a slice or an array of
    scalars, as described below.

    In the case where `i` is a slice or an array, `__getitem__(i)` should still
    return a single tuple.  The `j`th element of this tuple should be an array
    consisting of the `j`th elements of all the examples at indices `i`,
    stacked along the fisrt dim.  Thus in the common case where an example is
    `(x, y)` where `x` is a vector ("data") and `y` is a (scalar) label, the
    return value of `__getitem__(i)` where `i` is a slice or 1d array will be a
    pair `(X, y)` where `X` is a matrix whose rows are "data" vectors and `y`
    will be a vector whose elements are the labels corresponding to those rows.
    """

    def __init__(self, transforms: Optional[List] = None):
        self.transforms = transforms

    def __getitem__(self, index) -> object:
        raise NotImplementedError

    def __len__(self) -> int:
        raise NotImplementedError

    def apply_transforms(self, x):
        if self.transforms is not None:
            # apply the transforms
            for tform in self.transforms:
                x = tform(x)
        return x


class DataLoader:
    r"""
    Iterable sampler interface for a dataset.

    (I disagree with the class name, it's not a loader; but I didn't choose
    it.)

    Each item in the iterator is a mini-batch consisting of `batch_size`
    examples.  Each mini-batch is a tuple of arrays, stacked as described in
    the documentation of `Dataset`.

    If `shuffle` is false (the default), iteration passes through the dataset
    once in order.  If `shuffle` is true, each minibatch is chosen uniformly at
    random from the whole dataset, and the iterator never ends.

    Note that in the case where `shuffle` is false (the default), iteration is
    non-reentrant: only one sequential iterator through this collection can be
    held at a time.  (This would be easy to change in the future if needed.)

    Args:
        dataset: (`Dataset`) Dataset from which to load the data.
        batch_size: (`int`) How many examples per batch (default: 1).
        shuffle: (`bool`) If true, the data are reshuffled in every batch and the
            iterator never ends (default: false).
     """
    dataset: Dataset
    batch_size: Optional[int]

    def __init__(
        self,
        dataset: Dataset,
        batch_size: Optional[int] = 1,
        shuffle: bool = False,
    ):

        self.dataset = dataset
        self.shuffle = shuffle
        self.batch_size = batch_size

    def fresh_sequential_idxlists(self):
        """Returns a list-of-lists, each element of which is the list of scalar
        indices corresponding to the examples in a batch.  Together the batches
        signified by these indices constitute a sequential traversal of the
        dataset, where every batch except possibly the last one has size
        `batch_size`.
        """
        return np.array_split(np.arange(len(self.dataset)),
                              range(self.batch_size,
                                    len(self.dataset),
                                    self.batch_size))

    def __iter__(self):
        ### BEGIN YOUR SOLUTION
        if self.shuffle:
            self.idxlist_iter = None
        else:
            self.idxlist_iter = iter(self.fresh_sequential_idxlists())
        ### END YOUR SOLUTION
        return self

    def __next__(self):
        ### BEGIN YOUR SOLUTION
        if self.idxlist_iter is None:
            rng = np.random.default_rng()
            idxlist = rng.permutation(len(self.dataset))[:self.batch_size]
        else:
            idxlist = next(self.idxlist_iter)
        return tuple(map(Tensor, self.dataset[idxlist]))
        ### END YOUR SOLUTION


def _parse_mnist_labels(filename):
    with gzip.open(filename, "rb") as f:
        buf = f.read()
    (magic_number,) = struct.unpack_from(">i", buf, offset=0)
    assert magic_number == 0x00000801, hex(magic_number)
    (num_labels,) = struct.unpack_from(">i", buf, offset=4)
    assert num_labels >= 0 and num_labels < 1e14
    return np.frombuffer(buf, dtype=">u1", count=num_labels, offset=8)


def _parse_mnist_images(filename):
    with gzip.open(filename, "rb") as f:
        buf = f.read()
    (magic_number,) = struct.unpack_from(">i", buf, offset=0)
    assert magic_number == 0x00000803, hex(magic_number)
    (num_images, num_rows, num_cols) = struct.unpack_from(">iii", buf, offset=4)
    assert num_images >= 0 and num_images < 1e14
    assert num_rows == 28 and num_cols == 28
    X_unnormalized =  np.frombuffer(buf, dtype=">u1", offset=16,
                                    count=(num_images * num_rows * num_cols),
                                   ).reshape((-1, num_rows * num_cols),
                                             order="C")
    m = np.max(X_unnormalized)
    X = X_unnormalized.astype(np.float32) / (m if m != 0 else 1)
    return X


def parse_mnist(image_filename, label_filename):
    """ Read an images and labels file in MNIST format.  See this page:
    http://yann.lecun.com/exdb/mnist/ for a description of the file format.

    Args:
        image_filename (str): name of gzipped images file in MNIST format
        label_filename (str): name of gzipped labels file in MNIST format

    Returns:
        Tuple (X,y):
            X (numpy.ndarray[np.float32]): 2D numpy array containing the loaded
                data.  The dimensionality of the data should be
                (num_examples x input_dim) where 'input_dim' is the full
                dimension of the data, e.g., since MNIST images are 28x28, it
                will be 784.  Values should be of type np.float32, and the data
                should be normalized to have a minimum value of 0.0 and a
                maximum value of 1.0.

            y (numpy.ndarray[dypte=np.int8]): 1D numpy array containing the
                labels of the examples.  Values should be of type np.int8 and
                for MNIST will contain the values 0-9.
    """
    ### BEGIN YOUR SOLUTION
    X = _parse_mnist_images(image_filename)
    y = _parse_mnist_labels(label_filename)
    assert np.shape(X)[0] == np.shape(y)[0]
    return (X, y)
    ### END YOUR SOLUTION

class MNISTDataset(Dataset):
    """`Dataset` whose examples are labelled images from the MNIST dataset.

    Loads its data from MNIST data files supplied to the constructor.
    """
    def __init__(
        self,
        image_filename: str,
        label_filename: str,
        transforms: Optional[List] = None,
    ):
        ### BEGIN YOUR SOLUTION
        (self.imgs, self.labels) = parse_mnist(image_filename, label_filename)
        self.transforms = (transforms or [])
        ### END YOUR SOLUTION

    def __getitem__(self, index) -> object:
        ### BEGIN YOUR SOLUTION
        # I was only able to find the required API for this by looking at the
        # unit tests (including the DataLoader tests)...
        return (reduce(lambda img, tform: tform(img),
                       self.transforms,
                       self.imgs[index, :].reshape((-1, 28, 28, 1))
                       ).reshape((-1, 784,)),
                self.labels[index])
        ### END YOUR SOLUTION

    def __len__(self) -> int:
        ### BEGIN YOUR SOLUTION
        return self.imgs.shape[0]
        ### END YOUR SOLUTION

class NDArrayDataset(Dataset):
    def __init__(self, *arrays):
        self.arrays = arrays

    def __len__(self) -> int:
        return self.arrays[0].shape[0]

    def __getitem__(self, i) -> object:
        return tuple([a[i] for a in self.arrays])
