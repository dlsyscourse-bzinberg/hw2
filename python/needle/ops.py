"""Operatpr table."""
# Global operator table.
from numbers import Number
from itertools import count
from typing import Optional, List
from .autograd import NDArray
from .autograd import Op, Tensor, Value, TensorOp
from .autograd import TensorTuple, TensorTupleOp
import numpy
import operator

# NOTE: we will numpy as the array_api
# to backup our computations, this line will change in later homeworks
import numpy as array_api


class MakeTensorTuple(TensorTupleOp):
    def compute(self, *args) -> tuple:
        return tuple(args)

    def gradient(self, out_grad, node):
        assert isinstance(out_grad, TensorTuple)
        return tuple(*[out_grad[i] for i in range(len(out_grad))])


def make_tuple(*args):
    return MakeTensorTuple()(*args)


class TupleGetItem(TensorOp):
    def __init__(self, index):
        self.index = index

    def __call__(self, a: TensorTuple, fold_const=True) -> Value:
        assert isinstance(a, TensorTuple)
        # constant folding
        if fold_const and isinstance(a.op, MakeTensorTuple):
            return a.inputs[self.index]
        return Tensor.make_from_op(self, [a])

    def compute(self, a):
        return a[self.index]

    def gradient(self, out_grad, node):
        index = self.index
        in_grad = []
        for i, value in enumerate(node.inputs[0]):
            if i != index:
                in_grad.append(zeros_like(value))
            else:
                in_grad.append(out_grad)
        return MakeTensorTuple()(*in_grad)


def tuple_get_item(value, index):
    return TupleGetItem(index)(value)


class FusedAddScalars(TensorTupleOp):
    def __init__(self, c0: float, c1: float):
        self.c0 = c0
        self.c1 = c1

    def compute(self, a):
        return a + self.c0, a + self.c1

    def gradient(self, out_grad, node):
        return out_grad[0] + out_grad[1]


def fused_add_scalars(x, c0, c1):
    return FusedAddScalars(c0, c1)(x)


class EWiseAdd(TensorOp):
    def compute(self, a: NDArray, b: NDArray):
        return a + b

    def gradient(self, out_grad: Tensor, node: Tensor):
        return out_grad, out_grad


def add(a, b):
    return EWiseAdd()(a, b)


class AddScalar(TensorOp):
    def __init__(self, scalar):
        self.scalar = scalar

    def compute(self, a: NDArray):
        return a + self.scalar

    def gradient(self, out_grad: Tensor, node: Tensor):
        return out_grad


def add_scalar(a, scalar):
    return AddScalar(scalar)(a)


class EWiseMul(TensorOp):
    def compute(self, a: NDArray, b: NDArray):
        return a * b

    def gradient(self, out_grad: Tensor, node: Tensor):
        lhs, rhs = node.inputs
        return out_grad * rhs, out_grad * lhs


def multiply(a, b):
    return EWiseMul()(a, b)


class MulScalar(TensorOp):
    def __init__(self, scalar):
        self.scalar = scalar

    def compute(self, a: NDArray):
        return a * self.scalar

    def gradient(self, out_grad: Tensor, node: Tensor):
        return (out_grad * self.scalar,)


def mul_scalar(a, scalar):
    return MulScalar(scalar)(a)


class PowerScalar(TensorOp):
    """Op to raise a tensor to a (scalar) power."""

    def __init__(self, exponent: int):
        self.exponent = exponent

    def compute(self, a: NDArray) -> NDArray:
        ### BEGIN YOUR SOLUTION
        return array_api.power(a, self.exponent)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        # TODO: Add a ZerosLike tensor op and use it in the case where
        # `self.exponent == 0`.  Not necessary but nicer.
        (a,) = node.inputs
        deriv = multiply(
                    mul_scalar(power_scalar(a, self.exponent - 1),
                               self.exponent),
                    out_grad)
        return (deriv,)
        ### END YOUR SOLUTION


def power_scalar(a, scalar):
    return PowerScalar(scalar)(a)


class EWiseDiv(TensorOp):
    """Op to element-wise divide two nodes."""

    def compute(self, a, b):
        ### BEGIN YOUR SOLUTION
        return array_api.divide(a, b)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        (a, b) = node.inputs
        deriv_a = divide(out_grad, b)
        deriv_b = negate(divide(multiply(a, out_grad),
                                power_scalar(b, 2)))
        return (deriv_a, deriv_b)
        ### END YOUR SOLUTION


def divide(a, b):
    return EWiseDiv()(a, b)


class DivScalar(TensorOp):
    def __init__(self, scalar):
        self.scalar = scalar

    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        return array_api.divide(a, self.scalar)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        return divide_scalar(out_grad, self.scalar)
        ### END YOUR SOLUTION


def divide_scalar(a, scalar):
    return DivScalar(scalar)(a)


class Transpose(TensorOp):
    def __init__(self, axes: Optional[tuple] = None):
        self.axes = axes

    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        if self.axes is None:
            ax1 = array_api.ndim(a) - 2
            ax2 = array_api.ndim(a) - 1
        else:
            (ax1, ax2) = self.axes
        return array_api.swapaxes(a, ax1, ax2)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        return transpose(out_grad, axes=self.axes)
        ### END YOUR SOLUTION


def transpose(a, axes=None):
    return Transpose(axes)(a)


class Reshape(TensorOp):
    def __init__(self, shape):
        self.shape = shape

    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        return array_api.reshape(a, self.shape)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        assert out_grad.shape == self.shape
        (a,) = node.inputs
        old_shape = array_api.shape(a)
        return reshape(out_grad, old_shape)
        ### END YOUR SOLUTION


def reshape(a, shape):
    return Reshape(shape)(a)


def _grown_dims(new_shape, old_shape):
    """ Returns the indices of the dims in `new_shape` that are larger than
    their counterparts in `old_shape`.

    Requires that `old_shape` can be broadcast to `new_shape`.  The returned
    list contains the indices of the dims that are 1 (explicitly or implicitly)
    in `old_shape` and are more than 1 in `new_shape`.

    >>> _grown_dims((4, 5, 6, 7), (5, 1, 7))
    (0, 2)
    """
    pad_size = len(new_shape) - len(old_shape)
    padded_old_shape = operator.concat(pad_size * (1,),
                                       old_shape)
    return tuple(i for (i, old_dim, new_dim) in zip(
                      count(), padded_old_shape, new_shape)
                 if old_dim == 1 and new_dim != 1)


class BroadcastTo(TensorOp):
    def __init__(self, shape):
        self.shape = shape

    def compute(self, a):
        return array_api.broadcast_to(a, self.shape)

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        (a,) = node.inputs
        old_shape = array_api.shape(a)
        return reshape(summation(out_grad,
                                 axes=_grown_dims(self.shape, old_shape)),
                       old_shape)
        ### END YOUR SOLUTION


def broadcast_to(a, shape):
    return BroadcastTo(shape)(a)


class Summation(TensorOp):
    def __init__(self, axes: Optional[tuple] = None):
        if isinstance(axes, Number):
            self.axes = (axes,)
        else:
            self.axes = axes

    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        return array_api.sum(a, axis=self.axes)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        (a,) = node.inputs
        old_shape = array_api.shape(a)
        if node.op.axes is None:
            expanded_new_shape = len(old_shape) * (1,)
        else:
            _s = list(old_shape)
            for ax in node.op.axes:
                _s[ax] = 1
            expanded_new_shape = tuple(_s)
        return broadcast_to(reshape(out_grad,
                                    expanded_new_shape),
                            old_shape)
        ### END YOUR SOLUTION


def summation(a, axes=None):
    return Summation(axes)(a)


class MatMul(TensorOp):
    def compute(self, a, b):
        ### BEGIN YOUR SOLUTION
        return array_api.matmul(a, b)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        (a, b) = node.inputs

        out_shape = array_api.shape(out_grad)
        a_shape = array_api.shape(a)
        b_shape = array_api.shape(b)

        deriv_a = reshape(
                      summation(matmul(out_grad, transpose(b)),
                                axes=_grown_dims(out_shape, a_shape)),
                      a_shape)
        deriv_b = reshape(
                      summation(matmul(transpose(a), out_grad),
                                axes=_grown_dims(out_shape, b_shape)),
                      b_shape)
        return (deriv_a, deriv_b)
        ### END YOUR SOLUTION


def matmul(a, b):
    return MatMul()(a, b)


class Negate(TensorOp):
    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        return array_api.negative(a)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        return negate(out_grad)
        ### END YOUR SOLUTION


def negate(a):
    return Negate()(a)


class Log(TensorOp):
    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        return array_api.log(a)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        (a,) = node.inputs
        return divide(out_grad, a)
        ### END YOUR SOLUTION


def log(a):
    return Log()(a)


class Exp(TensorOp):
    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        return array_api.exp(a)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        return multiply(node, out_grad)
        ### END YOUR SOLUTION


def exp(a):
    return Exp()(a)


class ReLU(TensorOp):
    def compute(self, a):
        ### BEGIN YOUR SOLUTION
        return array_api.maximum(a, 0)
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        ## WARNING: Higher-order derivatives of this op will be silently
        ## incorrect, as explained in the homework question statement.
        return Tensor(node.numpy() > 0) * out_grad
        ### END YOUR SOLUTION


def relu(a):
    return ReLU()(a)



class LogSumExp(TensorOp):
    def __init__(self, axes: Optional[tuple] = None):
        self.axes = axes

    def compute(self, Z):
        ### BEGIN YOUR SOLUTION
        m = array_api.max(Z, axis=self.axes)
        # In both cases below, `m_` is broadcast-compatible with `Z`.
        if self.axes is None:
            m_ = m
        else:
            m_ = array_api.expand_dims(m, axis=self.axes)

        return m + array_api.log(
                       array_api.sum(
                           array_api.exp(Z - m_),
                           axis=self.axes))
        ### END YOUR SOLUTION

    def gradient(self, out_grad, node):
        ### BEGIN YOUR SOLUTION
        (a,) = node.inputs

        old_shape = array_api.shape(a)
        if self.axes is None:
            expanded_new_shape = len(old_shape) * (1,)
        else:
            _sh = list(old_shape)
            for ax in node.op.axes:
                _sh[ax] = 1
            expanded_new_shape = tuple(_sh)

        m = Tensor(array_api.reshape(array_api.max(a.numpy(), axis=self.axes),
                                     expanded_new_shape),
                   requires_grad=False)
        e = exp(a - m)
        s = summation(e, axes=self.axes)

        return (e
                * broadcast_to(reshape(out_grad, expanded_new_shape),
                               old_shape)
                / broadcast_to(reshape(s, expanded_new_shape),
                               old_shape))
        ### END YOUR SOLUTION


def logsumexp(a, axes=None):
    return LogSumExp(axes=axes)(a)
