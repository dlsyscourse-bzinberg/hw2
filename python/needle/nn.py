"""The module.
"""
from typing import List, Callable, Any
from needle.autograd import Tensor
from needle import ops
import needle.init as init
import numpy as np
from functools import reduce
from operator import concat


class Parameter(Tensor):
    """A special kind of tensor that represents parameters."""


def _unpack_params(value: object) -> List[Tensor]:
    if isinstance(value, Parameter):
        return [value]
    elif isinstance(value, Module):
        return value.parameters()
    elif isinstance(value, dict):
        params = []
        for k, v in value.items():
            params += _unpack_params(v)
        return params
    elif isinstance(value, (list, tuple)):
        params = []
        for v in value:
            params += _unpack_params(v)
        return params
    else:
        return []


def _child_modules(value: object) -> List["Module"]:
    if isinstance(value, Module):
        modules = [value]
        modules.extend(_child_modules(value.__dict__))
        return modules
    if isinstance(value, dict):
        modules = []
        for k, v in value.items():
            modules += _child_modules(v)
        return modules
    elif isinstance(value, (list, tuple)):
        modules = []
        for v in value:
            modules += _child_modules(v)
        return modules
    else:
        return []




class Module:
    def __init__(self):
        self.training = True

    def parameters(self) -> List[Tensor]:
        """Return the list of parameters in the module."""
        return _unpack_params(self.__dict__)

    def _children(self) -> List["Module"]:
        return _child_modules(self.__dict__)

    def eval(self):
        self.training = False
        for m in self._children():
            m.training = False

    def train(self):
        self.training = True
        for m in self._children():
            m.training = True

    def __call__(self, *args, **kwargs):
        return self.forward(*args, **kwargs)


class Identity(Module):
    def forward(self, x):
        return x


class Linear(Module):
    def __init__(self, in_features, out_features, bias=True, device=None, dtype="float32"):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features

        ### BEGIN YOUR SOLUTION
        self.weight = Parameter(
                          init.kaiming_uniform(in_features, out_features))
        self.bias = Parameter(
                        ops.reshape(init.kaiming_uniform(out_features, 1),
                                                         (out_features,)))
        ### END YOUR SOLUTION

    def forward(self, X: Tensor) -> Tensor:
        ### BEGIN YOUR SOLUTION
        # The homework question says `X` will have shape `(N, in_features)`,
        # but in some of the unit tests `X` has rank more than 2.  So we'll
        # handle that case too.
        (*_b, _input_length) = X.shape
        batch_dims = tuple(_b)
        assert _input_length == self.in_features
        return X @ self.weight + ops.broadcast_to(
                                     self.bias,
                                     concat(batch_dims, (self.out_features,)))
        ### END YOUR SOLUTION



class Flatten(Module):
    def forward(self, X):
        ### BEGIN YOUR SOLUTION
        (batch_dim, *rest_dims) = X.shape
        prod_rest_dims = reduce(lambda n, d: d * n, rest_dims, 1)
        return ops.reshape(X, (batch_dim, prod_rest_dims))
        ### END YOUR SOLUTION


class ReLU(Module):
    def forward(self, x: Tensor) -> Tensor:
        ### BEGIN YOUR SOLUTION
        return ops.relu(x)
        ### END YOUR SOLUTION


class Sequential(Module):
    def __init__(self, *modules):
        super().__init__()
        self.modules = modules

    def forward(self, x: Tensor) -> Tensor:
        ### BEGIN YOUR SOLUTION
        return reduce(lambda a, f: f(a),
                      self.modules,
                      x)
        ### END YOUR SOLUTION

def _mean(x: Tensor, axes=None):
    sh = x.shape
    if axes is None:
        axes = tuple(range(len(sh)))
    return ops.summation(x, axes=axes) / reduce(lambda n, i: n*sh[i],
                                                axes,
                                                1)

class SoftmaxLoss(Module):
    def forward(self, logits: Tensor, y: Tensor):
        ### BEGIN YOUR SOLUTION
        (*_b, num_classes) = logits.shape
        batch_dims = tuple(_b)
        assert y.shape == batch_dims, (logits.shape, y.shape)
        return _mean(
                   ops.logsumexp(logits, axes=(len(logits.shape) - 1,))
                   - ops.summation(logits * init.one_hot(num_classes, y),
                                   axes=(len(logits.shape) - 1,)))
        ### END YOUR SOLUTION



class BatchNorm1d(Module):
    def __init__(self, dim, eps=1e-5, momentum=0.1, device=None, dtype="float32"):
        super().__init__()
        self.dim = dim
        self.eps = eps
        self.momentum = momentum
        ### BEGIN YOUR SOLUTION
        self.weight = Parameter(init.ones(self.dim, device=device, dtype=dtype))
        self.bias = Parameter(init.zeros(self.dim, device=device, dtype=dtype))

        self.running_mean = init.zeros(self.dim, device=device, dtype=dtype)
        self.running_var = init.ones(self.dim, device=device, dtype=dtype)
        ### END YOUR SOLUTION


    def forward(self, x: Tensor) -> Tensor:
        ### BEGIN YOUR SOLUTION
        (*_b, _num_channels) = x.shape
        assert _num_channels == self.dim
        batch_dims = tuple(_b)
        batch_axes = tuple(range(len(batch_dims)))
        observed_mean = _mean(x, axes=batch_axes)
        # This method of computing variance has different numerics than the one
        # the unit tests assume you use
        #     observed_var = _mean(x**2, axes=batch_axes) - observed_mean**2
        observed_var = _mean((x - ops.broadcast_to(observed_mean, x.shape))**2,
                             axes=batch_axes)

        if self.training:
            self.running_mean = ((1 - self.momentum) * self.running_mean
                                 + self.momentum * observed_mean)
            # In principle, the coefficients for the variance update should
            # probably be
            #     (1 - momentum)^2 / (momentum^2 + (1 - momentum)^2)
            # and
            #     momentum^2 / (momentum^2 + (1 - momentum)^2)
            # but that does not appear to be what they want
            self.running_var = ((1 - self.momentum) * self.running_var
                                + self.momentum * observed_var)

            est_mean = observed_mean
            est_var = observed_var
        else:
            est_mean = self.running_mean
            est_var = self.running_var

        return (ops.broadcast_to(self.weight, x.shape)
                * (x - ops.broadcast_to(est_mean, x.shape))
                / ops.broadcast_to((est_var + self.eps)**0.5, x.shape)
                + ops.broadcast_to(self.bias, x.shape))
        ### END YOUR SOLUTION


class LayerNorm1d(Module):
    def __init__(self, dim, eps=1e-5, device=None, dtype="float32"):
        super().__init__()
        self.dim = dim
        self.eps = eps
        ### BEGIN YOUR SOLUTION
        self.weight = Parameter(init.ones(self.dim, device=device, dtype=dtype))
        self.bias = Parameter(init.zeros(self.dim, device=device, dtype=dtype))
        ### END YOUR SOLUTION

    def forward(self, x: Tensor) -> Tensor:
        ### BEGIN YOUR SOLUTION
        (num_examples, _num_channels) = x.shape
        assert _num_channels == self.dim
        channel_axes = (1,)
        mean = _mean(x, axes=channel_axes)
        var = _mean(x**2, axes=channel_axes) - mean**2
        return (ops.broadcast_to(self.weight, x.shape)
                * (x - ops.broadcast_to(ops.reshape(mean, (num_examples, 1)),
                                        x.shape))
                / ops.broadcast_to(ops.reshape((var + self.eps)**0.5,
                                               (num_examples, 1)),
                                   x.shape)
                + ops.broadcast_to(self.bias, x.shape))
        ### END YOUR SOLUTION


class Dropout(Module):
    def __init__(self, p = 0.5):
        super().__init__()
        self.p = p

    def forward(self, x: Tensor) -> Tensor:
        ### BEGIN YOUR SOLUTION
        if self.training:
            return init.randb(*x.shape, p=1-self.p) * x / (1 - self.p)
        else:
            return x
        ### END YOUR SOLUTION


class Residual(Module):
    def __init__(self, fn: Module):
        super().__init__()
        self.fn = fn

    def forward(self, x: Tensor) -> Tensor:
        ### BEGIN YOUR SOLUTION
        return self.fn(x) + x
        ### END YOUR SOLUTION



