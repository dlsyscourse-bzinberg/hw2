import sys
sys.path.append('../python')
import needle as ndl
from needle.data import DataLoader, MNISTDataset
import needle.nn as nn
import numpy as np
import time
import os

np.random.seed(0)

def ResidualBlock(dim, hidden_dim, norm=nn.BatchNorm1d, drop_prob=0.1):
    ### BEGIN YOUR SOLUTION
    return nn.Sequential(*[
        nn.Residual(nn.Sequential(*[
                        nn.Linear(dim, hidden_dim),
                        norm(hidden_dim),
                        nn.ReLU(),
                        nn.Dropout(drop_prob),
                        nn.Linear(hidden_dim, dim),
                        norm(dim),
                        ])),
        nn.ReLU(),
        ])
    ### END YOUR SOLUTION


def MLPResNet(dim, hidden_dim=100, num_blocks=3, num_classes=10,
              norm=nn.BatchNorm1d, drop_prob=0.1):
    ### BEGIN YOUR SOLUTION
    return nn.Sequential(*[
        nn.Linear(dim, hidden_dim),
        nn.ReLU(),
        *[ResidualBlock(hidden_dim, hidden_dim // 2,
                        norm=norm, drop_prob=drop_prob)
          for _ in range(num_blocks)],
        nn.Linear(hidden_dim, num_classes),
        ])
    ### END YOUR SOLUTION




def epoch(dataloader, model, opt=None, loss_func=nn.SoftmaxLoss()):
    np.random.seed(4)
    ### BEGIN YOUR SOLUTION
    if opt is None:
        model.eval()
    else:
        model.train()

    num_examples = 0
    num_batches = 0
    total_loss = 0
    error_count = 0
    for (X, y) in dataloader:
        out = model(X)
        loss = loss_func(out, y)
        if model.training:
            opt.reset_grad()
            loss.backward()
            opt.step()
        num_examples += X.shape[0]
        num_batches += 1
        total_loss += loss.numpy()
        error_count += np.sum(np.argmax(out.numpy(), axis=1) != y.numpy())

    error_rate = float(error_count) / num_examples
    avg_loss = float(total_loss) / num_batches
    return (error_rate, avg_loss)
    ### END YOUR SOLUTION



def train_mnist(batch_size=100, epochs=10, optimizer=ndl.optim.Adam,
                lr=0.001, weight_decay=0.001, hidden_dim=100, data_dir="data",
                image_filename="train-images-idx3-ubyte.gz",
                label_filename="train-labels-idx1-ubyte.gz"):
    np.random.seed(4)
    ### BEGIN YOUR SOLUTION
    dataloader = DataLoader(MNISTDataset(
                                os.path.join(data_dir, image_filename),
                                os.path.join(data_dir, label_filename)),
                            batch_size=batch_size,
                            shuffle=True)
    mnist_input_dim = 784
    model = MLPResNet(mnist_input_dim, hidden_dim)
    opt = optimizer(model.parameters(), lr=lr, weight_decay=weight_decay)

    error_rates = []
    avg_losses = []
    for _ in range(epochs):
        (error_rate, avg_loss) = epoch(dataloader, model, opt=opt)
        error_rates.append(error_rate)
        avg_losses.append(avg_loss)

    training_error = np.mean(error_rates[:-1])
    training_loss = np.mean(avg_losses[:-1])
    test_error = error_rates[-1]
    test_loss = avg_losses[-1]

    return (1 - training_error,
            training_loss,
            1 - test_error,
            test_loss)
    ### END YOUR SOLUTION



if __name__ == "__main__":
    train_mnist(data_dir="../data")
